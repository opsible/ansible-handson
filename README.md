# Ansible Hands-On Lab - bit.ly/opsible-handson

To use this repository, you'd need at least 4GB _free_ RAM and 10GB _free_ disk
space. A decent modern laptop (8GB RAM, SSD should work reliably).

```bash
vagrant up

./handson.sh
```

Now you are in the handson lab environment, on the deployer machine. You can run
the scripts in the directory `/vagrant/scripts/` to see how keystone is set up
and configured with ansible.

**If you are participating the Opsible Ansible hands-on lab, do NOT run the
scripts!**

Happy hacking!

## Questions?

Contact me: mark@opsible.com


