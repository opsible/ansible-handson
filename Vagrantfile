# vim: fdm=marker fdc=2 fdl=0 fen

# Setting up /etc/hosts
# {{{
$hosts = <<SCRIPT
cat << EOF | sudo tee -a /etc/hosts
192.168.57.11 deployer
192.168.57.12 services01
192.168.57.21 keystone01
192.168.57.22 keystone02
EOF
SCRIPT
# }}}

# Preparing hosts to be able to run ansible
# {{{
$prepare = <<SCRIPT
export DEBIAN_FRONTEND=noninteractive
sudo apt-get -y update
sudo apt-get -y install python
SCRIPT
# }}}

# Create swap space. Memory is scarce, disk is not
# {{{
$swap = <<SCRIPT
sudo dd if=/dev/zero of=/swap.img bs=1M count=512 > /dev/null 2>&1
sudo mkswap /swap.img
sudo chmod 0600 /swap.img
sudo swapon /swap.img
SCRIPT
# }}}

Vagrant.configure(2) do |config|
    # Global vm configuration
    # {{{
    config.vm.box = "ubuntu/xenial64"
    config.vm.provider "virtualbox" do |vb|
        vb.cpus = 1
        # Xenial COM1 port logging
        vb.customize ["modifyvm", :id, "--uartmode1", "disconnected"]
        # Set disks as SSD (fixes OSX Virtualbox Ubuntu boot problems)
        vb.customize ["modifyvm", :id, "--natnet1", "192.168.10/24"]
    end
    config.vm.box_check_update = false
    config.vm.graceful_halt_timeout = 180
    config.vm.provision "file",
        source: "keys/id_rsa.pub",  destination: ".ssh/authorized_keys"
    config.vm.provision "shell", inline: $hosts
    config.vm.provision "shell", inline: $prepare
    config.vm.provision "shell", inline: $swap
    config.ssh.forward_agent = true
    # }}}

    # DEPLOYER - the deployer machine
    # {{{
    config.vm.define "deployer" do |cli|
        cli.vm.hostname = "deployer"
        cli.vm.provider "virtualbox" do |vb|
            vb.memory = "256"
            vb.name = "deployer"
        end
        cli.vm.network "private_network", ip: "192.168.57.11"
        cli.vm.provision "file",
            source: "keys/id_rsa",  destination: ".ssh/id_rsa"
        cli.vm.provision "shell", inline: "chmod 0600 .ssh/id_rsa"
    end
    # }}}

    # SERVICES01 - services01 for openstack (db, mq, lb, etc)
    # {{{
    config.vm.define "services01" do |services01|
        services01.vm.hostname = "services01"
        services01.vm.provider "virtualbox" do |vb|
            vb.memory = "768"
            vb.name = "services01"
        end
        services01.vm.network "private_network", ip: "192.168.57.12"
    end
    # }}}

    # KEYSTONE01 - keystone instance #1
    # {{{
    config.vm.define "keystone01" do |keystone01|
        keystone01.vm.hostname = "keystone01"
        keystone01.vm.provider "virtualbox" do |vb|
            vb.memory = "512"
            vb.name = "keystone01"
        end
        keystone01.vm.network "private_network", ip: "192.168.57.21"
    end
    # }}}

    # KEYSTONE02 - keystone instance #2
    # {{{
    config.vm.define "keystone02" do |keystone02|
        keystone02.vm.hostname = "keystone02"
        keystone02.vm.provider "virtualbox" do |vb|
            vb.memory = "512"
            vb.name = "keystone02"
        end
        keystone02.vm.network "private_network", ip: "192.168.57.22"
    end
    # }}}
end
