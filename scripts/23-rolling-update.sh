#!/usr/bin/env bash

cd "$HOME/keystone"

# Our load balance keystone service provides high availability in the sense that
# if one of the keystone services breaks down, with proper configuration the
# load balancer can forward requests to the healthy one.
#
# During playbook execution, tasks are being run paralelly, which means that 
# during an update, all of our keystone services can go down - providing us with
# broken public services.
#
# Ansible has a built-in method to prevent this; the serial directive tells a
# play to execute tasks on batch hosts. More to read here:
# http://docs.ansible.com/ansible/latest/playbooks_delegation.html#rolling-update-batch-size
#
# To implement this behavior, simply add the following line to our keystone
# playbook:
cat << EOF | patch -Nup1 -r- site.yml
--- site.yml.orig       2017-07-25 20:44:03.183726413 +0000
+++ site.yml    2017-07-25 20:44:14.011702669 +0000
@@ -21,3 +21,4 @@
     - keystone_region_id: "RegionOne"
   roles:
     - openstack-keystone
+  serial: 1
EOF

# And finally, run the playbook
ansible-playbook -i hosts -l services site.yml \
                 --start-at-task="Install keystone packages"

