#!/usr/bin/env bash

cd "$HOME/keystone"

# Create the setup task for keystone
cat << EOF > openstack-keystone/tasks/setup.yml
---

- name: Install keystone packages
  apt: name=keystone
  become: true
  when: "'keystone' in group_names"
  tags: [install, keystone]
EOF

# Run the playbook immediately
ansible-playbook -i hosts site.yml --start-at-task="Install keystone packages"

# Retrieve the default keystone config file
[ ! -f openstack-keystone/templates/keystone.conf.j2 ] \
    && scp keystone01:/etc/keystone/keystone.conf \
           openstack-keystone/templates/keystone.conf.j2

# Error! With scp, we cannot get the privileged file. But we have ansible to 
# rescue.
[ ! -f openstack-keystone/templates/keystone.conf.j2 ] \
    && ansible -i hosts -m fetch \
               -a "src=/etc/keystone/keystone.conf dest=openstack-keystone/templates/keystone.conf.j2 flat=yes" \
               -b keystone01

# Set up mysql connection strings and fernet tokens
cat << EOF | patch -f -Nup1 -r- openstack-keystone/templates/keystone.conf.j2
--- openstack-keystone/templates/keystone.conf.j2.orig  2017-07-25 20:10:11.123393289 +0000
+++ openstack-keystone/templates/keystone.conf.j2       2017-07-25 20:10:17.223378141 +0000
@@ -710,7 +710,7 @@
 # Deprecated group/name - [DEFAULT]/sql_connection
 # Deprecated group/name - [DATABASE]/sql_connection
 # Deprecated group/name - [sql]/connection
-#connection = <None>
+connection = mysql+pymysql://{{ keystone_db_username }}:{{ keystone_db_password }}@services01/{{ keystone_db_name }}

 # The SQLAlchemy connection string to use to connect to the slave database.
 # (string value)
@@ -2839,7 +2839,7 @@
 # setup. \`fernet\` tokens do not need to be persisted at all, but require that
 # you run \`keystone-manage fernet_setup\` (also see the \`keystone-manage
 # fernet_rotate\` command). (string value)
-#provider = fernet
+provider = fernet

 # Entry point for the token persistence backend driver in the
 # \`keystone.token.persistence\` namespace. Keystone provides \`kvs\` and \`sql\`
EOF

# Continue writing tasks
cat << EOF >> openstack-keystone/tasks/setup.yml

- name: Configure keystone
  template:
    src: keystone.conf.j2
    dest: /etc/keystone/keystone.conf
    mode: 0600
    owner: keystone
    group: keystone
  become: true
  when: "'keystone' in group_names"
  notify: Restart apache service
  tags: [configure, keystone]
EOF

# With a little spice, one can leverage dictionaries to bulk together
# commands. Look at the command and with_itmes sections.
#
# Another handy mechanism is the delegate_to directive. Using it combined with
# the run_once directive one can easily specify initialization tasks running
# on a selected host, only once.
#
# Read more about delegation here: 
# http://docs.ansible.com/ansible/latest/playbooks_delegation.html
cat << EOF >> openstack-keystone/tasks/setup.yml

- name: Initialize keystone credentials
  command: keystone-manage {{ item.action }} {{ item.args }}
  with_items:
    - {action: fernet_setup, args: '--keystone-user keystone --keystone-group keystone'}
    - {action: credential_setup, args: '--keystone-user keystone --keystone-group keystone'}
  become: true
  become_user: keystone
  when: "'keystone' in group_names"
  tags: [initialize, keystone]

- name: Initialize keystone database
  command: keystone-manage {{ item.action }} {{ item.args }}
  with_items:
    - {action: db_sync, args: ''}
    - action: bootstrap
      args: >
        --bootstrap-password "{{ keystone_admin_password }}"
        --bootstrap-admin-url http://{{ keystone_api_url }}:35357/v3/
        --bootstrap-internal-url http://{{ keystone_api_url }}:5000/v3/
        --bootstrap-public-url http://{{ keystone_api_url }}:5000/v3/
        --bootstrap-region-id "{{ keystone_region_id }}"
  become: true
  become_user: keystone
  run_once: true
  delegate_to: "{{ groups['keystone'] | first }}"
  tags: [initialize, keystone]
EOF

# As a last step, add the apache service restart handler
cat << EOF > openstack-keystone/handlers/main.yml
---

- name: Restart apache service
  service:
    name: apache2
    state: restarted
  become: true
EOF

# Add the variables to site.yml
cat << EOF | patch -Nup1 -r- site.yml
--- site.yml.orig       2017-07-25 20:25:30.121690642 +0000
+++ site.yml    2017-07-25 20:25:56.365665929 +0000
@@ -16,5 +16,8 @@
     - openstack_db_admin_username: "root"
     - openstack_db_admin_password: ""
     - keystone_db_password: "secret"
+    - keystone_admin_password: "secret"
+    - keystone_api_url: "{{ groups['services'] | first }}"
+    - keystone_region_id: "RegionOne"
   roles:
     - openstack-keystone
EOF

# And run the playbook
ansible-playbook -i hosts site.yml --start-at-task="Configure keystone"

