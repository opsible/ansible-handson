#!/usr/bin/env bash

cd "$HOME/keystone"

# The services and keystone* hosts differ in the installed services, therefore
# it makes sense to separate the tasks run on these hosts.
#
# Let's advance with the services setup. For this, we expand our main yml
# SInce this time we install multiple packages, we can make use of the 
# `with_items` directive
# http://docs.ansible.com/ansible/latest/playbooks_loops.html#standard-loops
cat << EOF | patch -Nup1 -r- keystone.yml
--- keystone.yml.orig   2017-07-25 18:53:21.862225575 +0000
+++ keystone.yml        2017-07-25 18:53:36.446181055 +0000
@@ -28,3 +28,13 @@
         name: chrony
         state: restarted
       become: true
+
+- hosts: services
+  tasks:
+    - name: Install database
+      apt:
+        name: "{{ item }}"
+      with_items:
+        - mariadb-server
+        - python-pymysql
+      become: true
EOF


cat << EOF > templates/99-openstack.cnf.j2
[mysqld]
bind-address = {{ ansible_all_ipv4_addresses[1] }}

default-storage-engine = innodb
innodb_file_per_table = on
max_connections = 4096
collation-server = utf8_general_ci
character-set-server = utf8
EOF

cat << EOF | patch -Nup1 -r- keystone.yml
--- keystone.yml.orig   2017-07-25 18:55:09.861892141 +0000
+++ keystone.yml        2017-07-25 18:55:15.313875093 +0000
@@ -38,3 +38,17 @@
         - mariadb-server
         - python-pymysql
       become: true
+    - name: Configure database
+      template:
+        src: 99-openstack.cnf.j2
+        dest: /etc/mysql/mariadb.conf.d/99-openstack.cnf
+        mode: 0644
+      notify: Restart MariaDB service
+      become: true
+
+  handlers:
+    - name: Restart MariaDB service
+      service:
+        name: mysql
+        state: restarted
+      become: true
EOF

# Run the playbook
ansible-playbook -i hosts keystone.yml
