#!/usr/bin/env bash

cd "$HOME/keystone"

# Did you realize that we are now installing and setting up a lot of different
# software, not just keystone? Ansible best practice suggest using site.yml
# as the source of all setup. Let's rename keystone.yml to site.yml, and vim
# automagically starts working with the linters!
mv keystone.yml site.yml

# Move forward with keystone. The openstack-keystone role would require keystone
# *and* services hosts to install and configure services on. There is a way
# how we can define this
cat << EOF | patch -Nup1 -r- site.yml
--- site.yml.orig       2017-07-25 19:56:09.781223075 +0000
+++ site.yml    2017-07-25 19:55:26.665292236 +0000
@@ -10,3 +10,7 @@
     - openstack_rabbitmq_password: "secret"
   roles:
     - openstack-services
+
+- hosts: services,keystone
+  roles:
+    - openstack-keystone
EOF

# Often roles encapsulate complex, lenghty tasks. It's recommended to split up
# long playbooks to smaller pieces. For this, the include statement is helpful
# http://docs.ansible.com/ansible/latest/include_module.html
cat << EOF > openstack-keystone/tasks/main.yml
---

- include: prerequisites.yml
- include: setup.yml
- include: apis.yml
EOF

touch openstack-keystone/tasks/prerequisites.yml
touch openstack-keystone/tasks/setup.yml
touch openstack-keystone/tasks/apis.yml

