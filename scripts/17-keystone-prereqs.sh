#!/usr/bin/env bash

cd "$HOME/keystone"

# Let's go forward with setting up the database for keystone. The database 
# configuration is only needed to be done once, from a services node. There-
# fore we can use `run_once` and `when` directives.
# http://docs.ansible.com/ansible/latest/playbooks_delegation.html#run-once
# http://docs.ansible.com/ansible/latest/playbooks_conditionals.html#the-when-statement
cat << EOF > openstack-keystone/tasks/prerequisites.yml
---

- name: Create the keystone database
  mysql_db:
    name: "{{ keystone_db_name }}"
    login_user: "{{ openstack_db_admin_username }}"
    login_password: "{{ openstack_db_admin_password }}"
  run_once: true
  become: true
  when: "'services' in group_names"
  tags: [configure, keystone, database]

- name: Grant acces to the keystone database
  mysql_user:
    name: "{{ keystone_db_username }}"
    password: "{{ keystone_db_password }}"
    host: '%'
    priv: "{{ keystone_db_name }}.*:ALL"
    login_user: "{{ openstack_db_admin_username }}"
    login_password: "{{ openstack_db_admin_password }}"
  run_once: true
  become: true
  when: "'services' in group_names"
  tags: [configure, keystone, database]
EOF

# As you can see, we make extensive use of variables here. Some of these
# variables definitely should have defaults; likely the `keystone_db_name` would
# be 'keystone' in 99% of the cases. We can store defaults in defaults/main.yml
# Specifying defaults for as many of our variables as possible is advised at the
# role level.
cat << EOF > openstack-keystone/defaults/main.yml
---
# defaults file for openstack-keystone

keystone_db_name: keystone
keystone_db_username: keystone
EOF

# All the other values should be coming from the playbook level.
cat << EOF | patch -Nup1 -r- site.yml
--- site.yml.orig       2017-07-25 19:59:33.404850521 +0000
+++ site.yml    2017-07-25 19:59:39.436838506 +0000
@@ -12,5 +12,9 @@
     - openstack-services

 - hosts: services,keystone
+  vars:
+    - openstack_db_admin_username: "root"
+    - openstack_db_admin_password: ""
+    - keystone_db_password: "secret"
   roles:
     - openstack-keystone
EOF

# Note: it is very much advised that sensitive data, like passwords are stored
# in an encrypted format. Ansible-vault can be of help for that:
# http://docs.ansible.com/ansible/latest/playbooks_vault.html
#
# Let's run the tasks!
ansible-playbook -i hosts site.yml \
                 --start-at-task="Create the keystone database"

# It did not work. The reason is, that the MySQLdb python module is not
# installed on the services host. The `mysql_db` ansible module lists as a
# prerequisite, however it's easy to look over it. Let's find a solution in 
# the next step

