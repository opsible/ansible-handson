#!/usr/bin/env bash

cd "$HOME/keystone"

# Let's advance with the OpenStack installation according to the docs.
# Modules to read upon:
# http://docs.ansible.com/ansible/latest/apt_repository_module.html
cat << EOF | patch -Nup1 -r- keystone.yml
--- keystone.yml.orig   2017-07-25 18:46:33.671380242 +0000
+++ keystone.yml        2017-07-25 18:46:36.935371927 +0000
@@ -13,6 +13,14 @@
         mode: 0644
       become: true
       notify: Restart NTP service
+    - name: Install the ubuntu cloud keyring
+      apt:
+        name: ubuntu-cloud-keyring
+      become: true
+    - name: Configure ocata repositiories
+      apt_repository:
+        repo: 'deb http://ubuntu-cloud.archive.canonical.com/ubuntu xenial-updates/ocata main'
+      become: true

   handlers:
     - name: Restart NTP service
EOF

# Run the playbook
ansible-playbook -i hosts keystone.yml
