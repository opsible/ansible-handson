#!/usr/bin/env bash

cd "$HOME/keystone"

# To resolve the problem with the missing python MySQLdb dependency, the obvious
# solution would be just creating an apt task before the mysql tasks.  However,
# it's time to think about usage and usability of the roles.
#
# The keystone role already depends on the openstack-services role. We also can
# expect that other openstack services' ansible roles would require the same 
# MySQLdb. Thus, we have multiple possible solutions:
# 1. Don't overengineer it, use the openstack-keystone role to install mysql 
#    dependencies. Later, if it's needed, we would refactor
# 2. We most probably want to set-up glance too. Let's move the requirement 
#    installation to the openstack-services.
# 3. The database configuration step will appear in ALL openstack service
#    installation roles. Probably we should write a separate, parameterizable
#    role for it.
#
# The cleanest and most maintainable solution is 3. However, for the sake of 
# simplicity of this tutorial, we go with the quick-and-dirty 1.
cat << EOF | patch -Nup1 -r- openstack-keystone/tasks/prerequisites.yml
--- openstack-keystone/tasks/prerequisites.yml.orig     2017-07-25 20:02:08.896526565 +0000
+++ openstack-keystone/tasks/prerequisites.yml  2017-07-25 20:02:16.432510186 +0000
@@ -1,5 +1,10 @@
 ---

+- name: Install mysqldb dependency
+  apt: name=python-mysqldb
+  become: true
+  when: "'services' in group_names"
+
 - name: Create the keystone database
   mysql_db:
     name: "{{ keystone_db_name }}"
EOF

# Run the playbook again. This time it should run without any errors.
ansible-playbook -i hosts site.yml \
                 --start-at-task="Install mysqldb dependency"

