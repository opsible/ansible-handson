#!/usr/bin/env bash

cd "$HOME/keystone"

# Where does {{ ansible_all_ipv4_addresses[1] }} come from?
# Good practice is to list available facts using the setup module:
# http://docs.ansible.com/ansible/latest/setup_module.html
#
# NOTE: here we are not running a playbook, but invoking ansible directly, 
# therefore the use of command `ansible`
ansible -i hosts services -m setup

# With some easy sed magic we can transform the output to json only and feed 
# it to external tools like `jq` for processing
sudo apt -y install jq
ansible -i hosts services -m setup \
    | sed '1s/.*/{/g' \
    | jq -r '.ansible_facts.ansible_all_ipv4_addresses[1]'

# This is not much magic at all
# sed '1s/.*/{/'
#      || |  |
#      || |  `--- Replace with the object literal starter '{' character
#      || `------ Match the whole line
#      |`-------- Replace (mnemonic: substitute)
#      `--------- Operate on the first line only
#
# There are other variables that can come handy. Have a look on the debug
# module which helps us debug any variable or registered fact:
# http://docs.ansible.com/ansible/latest/debug_module.html
ansible -i hosts all -m debug -a var=vars
ansible -i hosts all -m debug -a var=environment
ansible -i hosts all -m debug -a var=group_names
ansible -i hosts all -m debug -a var=groups
ansible -i hosts all -m debug -a var=hostvars

# What are `all` and `services` here? `all` is a reserved special limiting 
# variable, means all the hosts in the inventory. `services` limits the
# module to be run on the [services] category of servers.
#
# You can even limit the scope of a playbook to a certain set of servers like
# this: `ansible-playbook -i hosts -l services keystone.yml`
