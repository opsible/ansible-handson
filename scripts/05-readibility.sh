#!/usr/bin/env bash

cd "$HOME/keystone"

# It gets very hard to read the inline module configuration in a longer
# playbook. It is advised to use one parameter per line
cat << EOF > keystone.yml
---

- hosts: all
  tasks:
    - name: Install chrony
      apt:
        name: chrony
      become: true
    - name: Configure chrony
      template:
        src: chrony.conf.j2
        dest: /etc/chrony/chrony.conf
        mode: 0644
      become: true
    - name: Restart NTP service
      service:
        name: chrony
        state: restarted
      become: true
EOF

# Run the playbook to check whether this configuration is equivalent
ansible-playbook -i hosts keystone.yml
