#!/usr/bin/env bash

cd "$HOME/keystone"

# Move forward with memcached.
cat << EOF | patch -Nup1 -r- keystone.yml
--- keystone.yml.orig   2017-07-25 19:03:35.476250606 +0000
+++ keystone.yml        2017-07-25 19:03:41.252231344 +0000
@@ -62,6 +62,13 @@
             read_priv: .*
             write_priv: .*
       become: true
+    - name: Install memcached
+      apt:
+        name: "{{ item }}"
+      with_items:
+        - memcached
+        - python-memcache
+      become: true

   handlers:
     - name: Restart MariaDB service
EOF

# Run the playbook
ansible-playbook -i hosts -l services keystone.yml

# Retrieve sample memcached config and convert to jinja template
[ ! -f templates/memcached.conf.j2 ] \
    && scp services01:/etc/memcached.conf \
           templates/memcached.conf.j2

cat << EOF | patch -Nup1 -r- templates/memcached.conf.j2
--- templates/memcached.conf.j2.orig    2017-07-25 19:06:47.799605392 +0000
+++ templates/memcached.conf.j2 2017-07-25 19:06:38.927635311 +0000
@@ -32,7 +32,7 @@
 # Specify which IP address to listen on. The default is to listen on all IP addresses
 # This parameter is one of the only security measures that memcached has, so make sure
 # it's listening on a firewalled interface.
--l 127.0.0.1
+-l {{ ansible_all_ipv4_addresses[1] }}

 # Limit the number of simultaneous incoming connections. The daemon default is 1024
 # -c 1024
EOF

cat << EOF | patch -Nup1 -r- keystone.yml
--- keystone.yml.orig   2017-07-25 19:32:34.964103122 +0000
+++ keystone.yml        2017-07-25 19:33:13.075995419 +0000
@@ -69,6 +69,13 @@
         - memcached
         - python-memcache
       become: true
+    - name: Configure memcached
+      template:
+        src: memcached.conf.j2
+        dest: /etc/memcached.conf
+        mode: 0644
+      become: true
+      notify: Restart memcached service

   handlers:
     - name: Restart MariaDB service
@@ -76,3 +83,8 @@
         name: mysql
         state: restarted
       become: true
+    - name: Restart memcached service
+      service:
+        name: memcached
+        state: restarted
+      become: true
EOF

# Run the playbook
ansible-playbook -i hosts -l services keystone.yml
