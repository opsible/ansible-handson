#!/usr/bin/env bash

# Configure ansible PPA repository and install ansible binary
sudo apt -y install software-properties-common
sudo add-apt-repository -y ppa:ansible/ansible
sudo apt -y update
sudo apt -y install ansible

# Create working directory
mkdir -p /vagrant/keystone
ln -s /vagrant/keystone "$HOME/"

