#!/usr/bin/env bash

cd "$HOME/keystone"

# Next step is installing keystone. It starts with database creation. What kind
# of ansible modules are around? Let's configure vim to bring us to help page!
#
# :nnoremap <leader>aa :!xdg-open http://docs.ansible.com/ansible/latest/list_of_all_modules.html<CR>
#
# Now start searching for mysql-related modules. Press `\aa`, the browser
# opens up with the "all modules" listing, Ctrl-F mysql, and voilá
#
# Obviously there are cases when we know that the required module is called
# `mysql_db`. So we would start writing the task:
cat << EOF > openstack-keystone/tasks/main.yml
- name: Create the keystone database
  mysql_db:
EOF

become
# But what are the parameters for `mysql_db` agin? Let's create another mapping:
#
# :nnoremap <leader>am :!xdg-open http://docs.ansible.com/ansible/latest/<C-R><C-W>_module.html\#options<CR>
#
# Position the cursor on the word mysql_db anywhere, `\am` and the `mysql_db`
# module help opens up. What if we just want a general google search on any
# keyword in our yaml file? Use this mapping:
#
# :nnoremap <leader>as :!xdg-open https://www.google.com/search?q=site:docs.ansible.com/ansible/latest/+\"<C-R><C-W>\"<CR>
#
# It's easy to remember these mnemonics:
# \aa - [a]nsible [a]ll modules
# \am - [a]nsible [m]odule
# \as - [a]nsible [s]earch
#
# You like it? persist in your vimrc:
mkdir -p ~/.vim/
cat << EOF >> ~/.vim/vimrc
nnoremap <leader>aa :!xdg-open http://docs.ansible.com/ansible/latest/list_of_all_modules.html<CR>
nnoremap <leader>am :!xdg-open http://docs.ansible.com/ansible/latest/<C-R><C-W>_module.html\#options<CR>
nnoremap <leader>as :!xdg-open https://www.google.com/search?q=site:docs.ansible.com/+\"<C-R><C-W>\"<CR>

EOF

# But vim gives you even more. For a somewhat customized setup, enabling basic
# linting, one can add the following configuration to their vimrc, and
# installing vim-plug and plugins
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cat << EOF >> ~/.vim/vimrc
call plug#begin('~/.vim/plugged')
Plug 'w0rp/ale'
Plug 'pearofducks/ansible-vim' , {'for': 'ansible*'}
call plug#end()

autocmd FileType ansible,yaml setlocal shiftwidth=2 softtabstop=2 tabstop=2
autocmd FileType ansible setlocal filetype=ansible.yaml
EOF
vim -c ':PlugInstall' -c ':qa'

# To make our setup work, it's advised to install yamllint and ansible-lint
sudo apt -y install python-pip
sudo -H pip install yamllint ansible-lint

# If you don't have vim 8, time to install it
vim --version | grep 'VIM - Vi IMproved 8.' >> /dev/null
[ $? -eq 1 ] \
    && sudo add-apt-repository -y ppa:jonathonf/vim \
    && sudo apt -y update \
    && sudo apt -y install vim

