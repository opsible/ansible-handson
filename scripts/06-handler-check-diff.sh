#!/usr/bin/env bash

cd "$HOME/keystone"

# As you could see re-running the current playbook always restarts the NTP
# service. It's better to only restart it when it's configuration actually
# changes.
#
# More on handlers and the notification mechanism:
# http://docs.ansible.com/ansible/latest/playbooks_intro.html#handlers-running-operations-on-change
#
# Let's write a handler that does this for us

# Move the NTP restart task to handlers and the notify directive upon config
# change.
cat << EOF | patch -Nup1 -r- keystone.yml
--- keystone.yml.orig   2017-07-25 18:44:19.631703769 +0000
+++ keystone.yml        2017-07-25 18:44:40.259656439 +0000
@@ -12,6 +12,9 @@
         dest: /etc/chrony/chrony.conf
         mode: 0644
       become: true
+      notify: Restart NTP service
+
+  handlers:
     - name: Restart NTP service
       service:
         name: chrony
EOF


# Run the playbook and watch how handlers are _NOT_ ran
ansible-playbook -i hosts keystone.yml

# Now try modifying the configuration for NTP by appending a line of comment at
# the end of the file
cat << EOF >> templates/chrony.conf.j2
# Harmless comment
EOF

# Before running the playbook to see that the handler restarts NTP, try out some
# debugging techniques. --check and --diff is a great help to see what kind of
# modifications would be done to the systems.
#
# --check only checks what would happen but does not execute anything
ansible-playbook -i hosts keystone.yml --check

# Together with --diff we also can see what changes would be applied
ansible-playbook -i hosts keystone.yml --check --diff

# OK, now run the playbook again. This time the handler will run and NTP will be
# restarted
ansible-playbook -i hosts keystone.yml
