#!/usr/bin/env bash

cd "$HOME/keystone"

# We are still using keystone.yml, however didn't even set up anything keystone
# related. Time to create roles!
ansible-galaxy init openstack-base
ansible-galaxy init openstack-services
ansible-galaxy init openstack-keystone

# Copy base tasks to the corresponding place
cat << EOF > openstack-base/tasks/main.yml
---
# tasks file for openstack-base
- name: Install chrony
  apt:
    name: chrony
  become: true
  tags: [install, ntp]
- name: Configure chrony
  template:
    src: chrony.conf.j2
    dest: /etc/chrony/chrony.conf
    mode: 0644
  become: true
  notify: Restart NTP service
  tags: [configure, ntp]
- name: Install the ubuntu cloud keyring
  apt:
    name: ubuntu-cloud-keyring
  become: true
- name: Configure ocata repositiories
  apt_repository:
    repo: 'deb http://ubuntu-cloud.archive.canonical.com/ubuntu xenial-updates/ocata main'
  become: true
EOF

# Copy base handlers to the corresponding place
cat << EOF > openstack-base/handlers/main.yml
---
# handlers file for openstack-base
- name: Restart NTP service
  service:
    name: chrony
    state: restarted
  become: true
EOF

# Move base templates to the corresponding place
mkdir -p openstack-base/templates
mv templates/chrony.conf.j2 openstack-base/templates/

# Copy services tasks to the corresponding place
cat << EOF > openstack-services/tasks/main.yml
---
# tasks file for openstack-services
- name: Install database
  apt:
    name: "{{ item }}"
  with_items:
    - mariadb-server
    - python-pymysql
  become: true
  tags: [install, database]
- name: Configure database
  template:
    src: 99-openstack.cnf.j2
    dest: /etc/mysql/mariadb.conf.d/99-openstack.cnf
    mode: 0644
  notify: Restart MariaDB service
  become: true
  tags: [configure, database]
- name: Install messagequeue
  apt:
    name: rabbitmq-server
  become: true
  tags: [install, messagequeue]
- name: Add rabbitmq user for openstack
  rabbitmq_user:
    user: "{{ openstack_rabbitmq_username }}"
    password: "{{ openstack_rabbitmq_password }}"
    permissions:
      - vhost: /
        configure_priv: .*
        read_priv: .*
        write_priv: .*
  become: true
  tags: [configure, messagequeue]
- name: Install memcached
  apt:
    name: "{{ item }}"
  with_items:
    - memcached
    - python-memcache
  become: true
  tags: [install, memcached]
- name: Configure memcached
  template:
    src: memcached.conf.j2
    dest: /etc/memcached.conf
    mode: 0644
  become: true
  tags: [configure, memcached]
  notify: Restart memcached service
EOF

# Copy services handlers to the corresponding place
cat << EOF > openstack-services/handlers/main.yml
---
# handlers file for openstack-services
- name: Restart MariaDB service
  service:
    name: mysql
    state: restarted
  become: true
- name: Restart memcached service
  service:
    name: memcached
    state: restarted
  become: true
EOF

# Re-create keystone.yml with playbooks that invoke roles
cat << EOF > keystone.yml
---

- hosts: all
  roles:
    - openstack-base

- hosts: services
  vars:
    - openstack_rabbitmq_username: "openstack"
    - openstack_rabbitmq_password: "secret"
  roles:
    - openstack-services
EOF

# Move services templates to the corresponding place
mkdir -p openstack-services/templates
mv templates/{99-openstack.cnf,memcached.conf}.j2 openstack-services/templates/

# Remove empty templates/ directory
rm -rf templates/

# Try to run the playbook again. If there wasn't any error in moving around yml
# configuration lines and files, it should run without any problems
ansible-playbook -i hosts keystone.yml
