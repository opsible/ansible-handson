#!/usr/bin/env bash

cd "$HOME/keystone"

# Ansible is capable not just running commands on remote systems but to trigger
# APIs. Obviously these plays need to be run after the services set up on the
# hosts. It is a good practice to set up your pipeline to run code againts APIs
# in a dedicated phase - the operations phase.
#
# Keystone, as all the openstack microservices, can be interacted with through
# a public ReST API. To make users' job easier, there are convenient interfaces
# that use these APIs: the `openstack` command line tool and horizon, a graphical
# user interface.
#
# The documentation instruct us to use the cli tools. Ansible also has built-in
# modules for interacting with the openstack. For those, see the following
# modules:
#
# http://docs.ansible.com/ansible/latest/os_project_module.html
# http://docs.ansible.com/ansible/latest/os_user_module.html
# http://docs.ansible.com/ansible/latest/os_keystone_role_module.html
#
# The scope of this tutorial is not to implement operational task, however,
# since it's a good example for using further ansible features, let's see how
# one can use it with OpenStack.
#
# Instead of using the builtin openstack or comment modules let's get familiar
# with the uri module which is general enough to be useful for many ReST-enabled
# services to operate.
# 
# Read about this curl replacement here:
# http://docs.ansible.com/ansible/latest/uri_module.html
#
# To understand the following tasks, one needs to get familiar with the openstack
# identity API. The API documentation can be found here:
# https://developer.openstack.org/api-ref/identity/v3/
#
# As in the previous section, we only run these oepratiional tasks only once, 
# and delegate the responsibility to the deployer host. New keywords include:
# - register: http://docs.ansible.com/ansible/latest/playbooks_variables.html
# - failed_when: http://docs.ansible.com/ansible/latest/playbooks_error_handling.html
#
# The first three tasks:
# 1. get an authentication token
# 2. print out a debug message with the variable
# 3. register the token under a convenient name
cat << EOF > openstack-keystone/tasks/apis.yml
---

- name: Get keystone api auth token
  uri:
    url: "http://{{ keystone_api_url }}:35357/v3/auth/tokens"
    method: POST
    headers:
      Content-Type: application/json
    body:
      auth:
        identity:
          methods:
            - password
          password:
            user:
              name: admin
              password: "{{ keystone_admin_password }}"
              domain:
                name: Default
        scope:
          project:
            domain:
              name: Default
            name: admin
    body_format: json
    return_content: true
  run_once: true
  delegate_to: localhost
  register: resp
  failed_when: "not resp.status == 201"
  tags: [keystone, api]

- debug: var=resp

- set_fact:
    token: "{{ resp.x_subject_token }}"
EOF

# Now that we have the authentication token, let's trigger one of the admin APIs
# According to the documentation, there are multiple status code that signal
# failure, while there is the status code 409 that signals that project creation
# "failed", because it was already existing.
#
# As a quick-and-dirty implementation we can say that our project was only
# created if the response code is not a failure code and neither 409.
#
# To decide whether a task actually changed systems, the `changed_when` directive
# is at our disposal.
cat << EOF >> openstack-keystone/tasks/apis.yml

- name: Create project
  uri:
    url: "http://{{ keystone_api_url }}:35357/v3/projects"
    method: POST
    headers:
      Content-Type: application/json
      X-Auth-Token: "{{ token }}"
    body:
      project:
        description: Demo project
        domain_id: default
        enabled: true
        name: demo
    body_format: json
    return_content: true
  run_once: true
  delegate_to: localhost
  register: resp
  changed_when: "resp.status != 409"
  failed_when: "resp.status in [400, 401, 403]"
  tags: [keystone, api]
EOF

# Finally we can run our playbook
ansible-playbook -i hosts -l services site.yml \
                 --start-at-task="Get keystone api auth token"
