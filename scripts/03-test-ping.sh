#!/usr/bin/env bash

cd "$HOME/keystone"

# Test connection to all hosts. This also tests whether hosts can run ansible
# tasks
ansible -i hosts all -m ping

