#!/usr/bin/env bash

cd "$HOME/keystone"

# Since we use two keystone hosts for the sake of dhowing how ansible can work
# with multiple targets, we'd want to set up load balancing on those hosts.
# The topic of load balancing is not in the scope of this tutorial. Using nginx
# one can easily establish a working setup.
#
# For the virtualhost configuration we can leverage the ansible `groups`
# variable which has all the hosts listed in a group. Iterating over them with a
# for loop would put all the keystone hosts in the jinja template.
#
# Jinja is quite powerful when it comes to similar scripting and templating
# tasks. To learn more, visit here:
# http://docs.ansible.com/ansible/latest/playbooks_templating.html
cat << EOF > openstack-keystone/templates/lb-keystone.j2
upstream keystone_admin {
    ip_hash;
{% for item in groups['keystone'] %}
    server {{ item }}:35357;
{% endfor %}
}

upstream keystone_public {
    ip_hash;
{% for item in groups['keystone'] %}
    server {{ item }}:5000;
{% endfor %}
}

server {
    listen 35357;

    location / {
        proxy_pass http://keystone_admin;
        proxy_redirect http://keystone_admin http://{{ keystone_api_url }};
        proxy_set_header Host \$host:\$server_port;
        proxy_pass_request_headers on;
    }
}

server {
    listen 5000;

    location / {
        proxy_pass http://keystone_public;
        proxy_redirect http://keystone_public http://{{ keystone_api_url }};
        proxy_set_header Host \$host:\$server_port;
        proxy_pass_request_headers on;
    }
}
EOF

# Since we don't have a separate loadbalancer host, we can choose to set up
# nginx the very first (and in our case only)  machine of the services group.
# Look at this line: `delegate_to:  "{{ groups['services'] | first}}"`
cat << EOF | patch -Nup1 -r- openstack-keystone/tasks/setup.yml
--- openstack-keystone/tasks/setup.yml.orig     2017-07-25 20:38:44.392404547 +0000
+++ openstack-keystone/tasks/setup.yml  2017-07-25 20:38:07.964478767 +0000
@@ -44,3 +44,19 @@
   run_once: true
   delegate_to: "{{ groups['keystone'] | first }}"
   tags: [initialize, keystone]
+
+- name: Install nginx packages
+  apt: name=nginx
+  become: true
+  delegate_to: "{{ groups['services'] | first}}"
+  tags: [install, nginx]
+
+- name: Configure nginx
+  template:
+    src: lb-keystone.j2
+    dest: /etc/nginx/sites-enabled/lb-keystone
+    mode: 0644
+  become: true
+  notify: Restart nginx service
+  delegate_to: "{{ groups['services'] | first}}"
+  tags: [configure, nginx]
EOF

# As usual, create the handler for restarting the service
cat << EOF | patch -Nup1 -r- openstack-keystone/handlers/main.yml
--- openstack-keystone/handlers/main.yml.orig   2017-07-25 20:39:52.436263819 +0000
+++ openstack-keystone/handlers/main.yml        2017-07-25 20:40:13.768219184 +0000
@@ -5,3 +5,8 @@
     name: apache2
     state: restarted
   become: true
+- name: Restart nginx service
+  service:
+    name: nginx
+    state: restarted
+  become: true
EOF

# And run the playbook
ansible-playbook -i hosts -l services site.yml \
                 --start-at-task="Install nginx packages"
