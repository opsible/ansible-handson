#!/usr/bin/env bash

cd "$HOME/keystone"

# As you write more uri tasks, you would find the work of putting the json
# request body into the playbook. Instead of having this document in-place, one
# can create a jinja template json file and lookup that file. To read more about
# the different lookup methods, lookup this site:
# http://docs.ansible.com/ansible/latest/playbooks_lookups.html
#
# First, create the json jinja template
cat << EOF > openstack-keystone/templates/req_keystone_token.json.j2
{
  "auth": {
    "identity": {
      "methods": [
        "password"
      ],
      "password": {
        "user": {
          "name": "admin",
          "password": "{{ keystone_admin_password }}",
          "domain": {
            "name": "Default"
          }
        }
      }
    },
    "scope": {
      "project": {
        "domain": {
          "name": "Default"
        },
        "name": "admin"
      }
    }
  }
}
EOF

# And now we can eliminate the lengthy in-line config.
cat << EOF | patch -Nup1 -r- openstack-keystone/tasks/apis.yml
--- openstack-keystone/tasks/apis.yml.orig      2017-07-25 20:42:28.515932299 +0000
+++ openstack-keystone/tasks/apis.yml   2017-07-25 20:42:35.531917153 +0000
@@ -6,22 +6,7 @@
     method: POST
     headers:
       Content-Type: application/json
-    body:
-      auth:
-        identity:
-          methods:
-            - password
-          password:
-            user:
-              name: admin
-              password: "{{ keystone_admin_password }}"
-              domain:
-                name: Default
-        scope:
-          project:
-            domain:
-              name: Default
-            name: admin
+    body: "{{ lookup('template', 'templates/req_keystone_token.json.j2') }}"
     body_format: json
     return_content: true
   run_once: true
EOF

# Run the playbook
ansible-playbook -i hosts -l services site.yml \
                 --start-at-task="Get keystone api auth token"

