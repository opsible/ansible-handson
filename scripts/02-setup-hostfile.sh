#!/usr/bin/env bash

cd "$HOME/keystone"

# Create hosts file
cat << EOF > hosts
[services]
services01

[keystone]
keystone01
keystone02
EOF
