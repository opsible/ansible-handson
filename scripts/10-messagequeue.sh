#!/usr/bin/env bash

cd "$HOME/keystone"

# Similar to the database, let's setup rabbitmq as the message queue service.
# RabbitMQ password is configured in this step. It is a good practice to factor
# out variable-like values to the `vars` section of the playbook
# Learn more about variables here:
# http://docs.ansible.com/ansible/latest/playbooks_variables.html
cat << EOF | patch -Nup1 -r- keystone.yml
--- keystone.yml.orig   2017-07-25 18:59:15.001109273 +0000
+++ keystone.yml        2017-07-25 18:59:21.769087255 +0000
@@ -30,6 +30,9 @@
       become: true

 - hosts: services
+  vars:
+    - openstack_rabbitmq_username: "openstack"
+    - openstack_rabbitmq_password: "secret"
   tasks:
     - name: Install database
       apt:
@@ -45,6 +45,20 @@
         mode: 0644
       notify: Restart MariaDB service
       become: true
+    - name: Install messagequeue
+      apt:
+        name: rabbitmq-server
+      become: true
+    - name: Add rabbitmq user for openstack
+      rabbitmq_user:
+        user: "{{ openstack_rabbitmq_username }}"
+        password: "{{ openstack_rabbitmq_password }}"
+        permissions:
+          - vhost: /
+            configure_priv: .*
+            read_priv: .*
+            write_priv: .*
+      become: true

   handlers:
     - name: Restart MariaDB service
EOF

# Run the playbook
ansible-playbook -i hosts -l services keystone.yml
