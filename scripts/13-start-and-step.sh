#!/usr/bin/env bash

cd "$HOME/keystone"

# During playbook creation, usually one wants to try out only the latest
# additions. Read more about the start-at-task feature here:
# http://docs.ansible.com/ansible/latest/playbooks_startnstep.html

# Let's say we only want to try out our latest addition, memcached installation
# and configuration
ansible-playbook -i hosts keystone.yml --start-at-task="Install memcached"

# Another option for gradually building up your playbook is to skip certain
# tasks. You can enable a step-by-step mode with --step
ansible-playbook -i hosts keystone.yml --step

# These two can be combined
ansible-playbook -i hosts keystone.yml --step --start-at-task="Install memcached"
