#!/usr/bin/env bash

cd "$HOME/keystone"

# As you can see, it's usual that we install and configure certain applications.
# Using tags, one can specify to only run a certain type of tasks.
# http://docs.ansible.com/ansible/latest/playbooks_tags.html
#
# Let's add the `configure` and `install` tags, along with the service types
cat << EOF | patch -Nup1 -r- keystone.yml
--- keystone.yml.orig   2017-07-25 19:45:15.645861678 +0000
+++ keystone.yml        2017-07-25 19:45:50.041757500 +0000
@@ -6,6 +6,7 @@
       apt:
         name: chrony
       become: true
+      tags: [install, ntp]
     - name: Configure chrony
       template:
         src: chrony.conf.j2
@@ -13,6 +14,7 @@
         mode: 0644
       become: true
       notify: Restart NTP service
+      tags: [configure, ntp]
     - name: Install the ubuntu cloud keyring
       apt:
         name: ubuntu-cloud-keyring
@@ -41,6 +43,7 @@
         - mariadb-server
         - python-pymysql
       become: true
+      tags: [install, database]
     - name: Configure database
       template:
         src: 99-openstack.cnf.j2
@@ -48,10 +51,12 @@
         mode: 0644
       notify: Restart MariaDB service
       become: true
+      tags: [configure, database]
     - name: Install messagequeue
       apt:
         name: rabbitmq-server
       become: true
+      tags: [install, messagequeue]
     - name: Add rabbitmq user for openstack
       rabbitmq_user:
         user: "{{ openstack_rabbitmq_username }}"
@@ -62,6 +67,7 @@
             read_priv: .*
             write_priv: .*
       become: true
+      tags: [configure, messagequeue]
     - name: Install memcached
       apt:
         name: "{{ item }}"
@@ -69,6 +75,7 @@
         - memcached
         - python-memcache
       become: true
+      tags: [install, memcached]
     - name: Configure memcached
       template:
         src: memcached.conf.j2
@@ -76,6 +83,7 @@
         mode: 0644
       become: true
       notify: Restart memcached service
+      tags: [configure, memcached]

   handlers:
     - name: Restart MariaDB service
EOF

# Run the playbook in different modes.
#
# Let's take care only ntp-related tasks
ansible-playbook -i hosts keystone.yml --tags ntp


# Or let's say we change something that requires reconfiguring multiple services
ansible-playbook -i hosts keystone.yml --tags configure

# During testing One usually don't want to always re-check whether package is
# installed. Using --skip-tags this is possible
ansible-playbook -i hosts keystone.yml --skip-tags configure
